# Linear Regression
- The process of drawing a line through data in a [scatter plot](#scatterplot).
- This line summarizes the data, which is useful when making predictions.
- There are advanced ways of fitting a line to data, but in general, we want the line to go through the "middle" of the points.
- Once we fit a line to data, we find that line's equation (linear equation) to make predictions.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="best_fit.PNG" style="width:75%">

## Example
The percent of adults who smoke, recorded every few years since 1976, suggests a negative linear assosciation with no outliers. Below is a line fit to the data to model the relationship.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="parents_smoking.PNG" style="width:75%">

We are going to write a linear equation to describe the given model.

### Step 1: Find the slope
This line goes through (0, 40) and (10, 35), so the slope is: <br><img src="slope0.PNG" style="width:20%">

### Step 2: Find the *y*-intercept
The line passess through (0, 40), so the *y*-intercept is 40.

### Step 3: Write the equation in *y=mx+b* form.
The equation is y = 0.5x + 40

Based on this equation, we can estimate what percent of adults smoked in 1997.

To do that we plug in 30 for *x* i.e 1997-1967 = 30yrs, where *x* represents years since 1967.

y = -0.5x + 40<br>
y = (-0.5)(30) + 40
y = -15 + 40
y = 25

Therefore, based on the equation, about 25% of adults smoked in 1997.


## Scatterplot
- aka  **scatter graph, scatter chart, scattergram, or scatterdiagram**
- uses cartesian coordinates

## Cartesian Coordinates 
- the points in a plane (surface in which if any two points are chosen a straight line joining them lies wholly in that surface; a flat or level surface) of line written as (x,y). 
- allow one to specify the location of a point in the plane
- the intersection of x and y axes is called the *origin*
- **x-coordinate**: 
    - signed distance from the origin in the direction of the x (horizontal)-axis.
    - specifies the distance to the right if x is +ve of the y-axis,
    - or to the left if x is -ve of the y-axis
- **y-coordinate**:
    - signed distance from the origin in the direction along the y (vertical)-axis

