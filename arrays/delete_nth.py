#! /usr/bin/python
"""
Given a list 'lst' and a number N, create a new list that contains each digit from the list appearing at most N times without reordering.

For example if N = 2, and the list is [1,2,3,1,2,1,2,3], the result should be [1,2,3,1,2], the next [1,2] should be dropped since 1 and 2 appear 3 times. The 2nd '3' is then included to the new list to give [1,2,3,1,2,3] 
"""

# collections module implements specialized container datatypes providing alternatives to general purpose built in containers: dict, list, set, tuple.
import collections

# class collections.defaultdict([default_factory[,...]])
# returns a new dictionary-like object.

# Time complexity 0(n^2)


def delete_nth_naive(array, n):
    ans = []
    for num in array:
        if ans.count(num) < n:
            ans.append(num)
    print(ans)
    return ans

# Time complexity 0(n), using hash tables


def delete_nth(array, n):
    result = []
    counts = collections.defaultdict(int)  # keep track of occurrences

    for i in array:
        if counts[i] < n:
            result.append(i)
            counts[i] += 1
    print(result)
    return result


if __name__ == "__main__":
    array = [1, 2, 3, 1, 2, 1, 2, 3]
    n = 2
    delete_nth_naive(array, n)
    delete_nth(array, n)
