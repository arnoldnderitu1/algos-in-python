# Bisection Method
A mathematically technique to finding numerical solutions of an equation with one unknown. 

The bisection method is used to find the <u>**roots of a polynomial**</u> equation:

> ## Polynomial:
> an expression consisting of variables (aka indeterminates) and coefficients (constant placed before and multiplies the variable in the expression), that involves only the operations of addition, subtraction, multiplication, and non-negative integer exponentiation of variables. <br><br> **Example**: a polynomial of a single indeterminate x <br><br> x^2 - 4x + 7 

> ## Roots of polynomials
> solutions for a given polynomial for which we need to find the value of the unknown variable. If we know the roots, we can evaluate the value of polynomial to zero.  